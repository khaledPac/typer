(*
 *      Typer Compiler
 *
 * ---------------------------------------------------------------------------
 *
 *      Copyright (C) 2011-2020  Free Software Foundation, Inc.
 *
 *   Author: Pierre Delaunay <pierre.delaunay@hec.ca>
 *   Keywords: languages, lisp, dependent types.
 *
 *   This file is part of Typer.
 *
 *   Typer is free software; you can redistribute it and/or modify it under the
 *   terms of the GNU General Public License as published by the Free Software
 *   Foundation, either version 3 of the License, or (at your option) any
 *   later version.
 *
 *   Typer is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 *   more details.
 *
 *   You should have received a copy of the GNU General Public License along
 *   with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ---------------------------------------------------------------------------
 *
 * Description:
 *
 * Elaborate Sexp expression into Lexp.
 * This includes type inference, and macro expansion.
 *
 * While elaboration will discover most type errors, strictly speaking
 * this phase does not need to perform any checks, and it can instead presume
 * that the code is well-behaved.  The real type checks (and totality
 * checks) are performed later in OL.check.
 *
 * -------------------------------------------------------------------------- *)

open Util
open Fmt

open Prelexer
open Lexer

open Sexp
open Pexp
open Lexp

open Env
open Debruijn
module DB = Debruijn
module M = Myers
module EV = Eval

open Grammar
module BI = Builtin

module Unif = Unification

module OL = Opslexp
module EL = Elexp

(* dummies *)
let dloc = dummy_location

let parsing_internals = ref false
let btl_folder =
  try Sys.getenv "TYPER_BUILTINS"
  with Not_found -> "./btl"

let fatal = Log.log_fatal ~section:"ELAB"
let error = Log.log_error ~section:"ELAB"
let warning = Log.log_warning ~section:"ELAB"
let info = Log.log_info ~section:"ELAB"

let indent_line str =
  "        > " ^ str
let print_indent_line str =
  print_endline (indent_line str)
let print_details to_name to_str elem =
  print_indent_line ((to_name elem) ^ ": " ^ (to_str elem))
let lexp_print_details lexp () =
  print_details lexp_name lexp_string lexp
let value_print_details value () =
  print_details value_name value_string value

let lexp_error loc lexp =
  error ~loc ~print_action:(lexp_print_details lexp)
let lexp_fatal loc lexp =
  fatal ~loc ~print_action:(lexp_print_details lexp)
let value_fatal loc value =
  fatal ~loc ~print_action:(value_print_details value)

(** Type info returned by elaboration.  *)
type sform_type =
  | Checked       (* Checked that the expression has the requested type.  *)
  | Inferred of ltype            (* Hasn't looked as the requested type.  *)
  | Lazy    (* Hasn't looked as the requested type, nor inferred a type.  *)

type special_forms_map =
  (elab_context -> location -> sexp list -> ltype option
   -> (lexp * sform_type)) SMap.t

let special_forms : special_forms_map ref = ref SMap.empty
let type_special_form = BI.new_builtin_type "Special-Form" type0

let add_special_form (name, func) =
  BI.add_builtin_cst name (mkBuiltin ((dloc, name), type_special_form));
  special_forms := SMap.add name func (!special_forms)

let get_special_form name =
  SMap.find name (!special_forms)

(* Used for sform_load because sform are
 * added before default context's function. *)
let sform_default_ectx = ref empty_elab_context

(* The prefix `elab_check_` is used for functions which do internal checking
 * (i.e. errors signalled here correspond to internal errors rather than
 * to errors in the user's code).  *)

let elab_check_sort (ctx : elab_context) lsort var ltp =
  match (try OL.lexp'_whnf lsort (ectx_to_lctx ctx)
         with e ->
           info ~print_action:(fun _ -> lexp_print lsort; print_newline ())
             ~loc:(lexp_location lsort)
             "Exception during whnf of sort:";
           raise e) with
  | Sort (_, _) -> () (* All clear!  *)
  | _ -> let typestr = lexp_string ltp ^ " : " ^ lexp_string lsort in
        match var with
        | (l, None) -> lexp_error l ltp
                        ("`" ^ typestr ^ "` is not a proper type")
        | (l, Some name)
          -> lexp_error l ltp
                       ("Type of `" ^ name ^ "` is not a proper type: "
                        ^ typestr)

let elab_check_proper_type (ctx : elab_context) ltp var =
  try elab_check_sort ctx (OL.check (ectx_to_lctx ctx) ltp) var ltp
  with e -> match e with
            | Log.Stop_Compilation _ -> raise e
            | _ ->
               info
                 ~print_action:(fun _ ->
                   print_lexp_ctx (ectx_to_lctx ctx); print_newline ()
                 )
                 ~loc:(lexp_location ltp)
                 ("Exception while checking type `" ^ (lexp_string ltp) ^ "`"
                  ^ (match var with
                     | (_, None) -> ""
                     | (_, Some name)
                       -> " of var `" ^ name ^ "`"));
               raise e

let elab_check_def (ctx : elab_context) var lxp ltype =
  let lctx = ectx_to_lctx ctx in
  let loc = lexp_location lxp in

  let ltype' = try OL.check lctx lxp
    with e -> match e with
      | Log.Stop_Compilation _ -> raise e
      | _ ->
         info
           ~print_action:(fun _ ->
             lexp_print_details lxp ();
             print_lexp_ctx (ectx_to_lctx ctx);
             print_newline ()
           )
           ~loc
           "Error while type-checking";
         raise e in
  if (try OL.conv_p (ectx_to_lctx ctx) ltype ltype'
      with e ->
        info ~print_action:(lexp_print_details lxp)
          ~loc
          ("Exception while conversion-checking types: "
           ^ lexp_string ltype ^ " and " ^ lexp_string ltype');
        raise e)
  then
    elab_check_proper_type ctx ltype var
  else
    fatal
      ~print_action:(fun _ ->
        List.iter print_indent_line [
            (match var with (_, Some n) -> n | _ -> "<anon>")
            ^ " = " ^ lexp_string lxp ^ " !: " ^ lexp_string ltype;
            "                    because";
            lexp_string ltype' ^ " != " ^ lexp_string ltype
      ])
      ~loc
      "Type check error: ¡¡ctx_define error!!"

let ctx_extend (ctx: elab_context) (var : vname) def ltype =
  elab_check_proper_type ctx ltype var;
  ectx_extend ctx var def ltype

let ctx_define (ctx: elab_context) var lxp ltype =
  elab_check_def ctx var lxp ltype;
  ectx_extend ctx var (LetDef (0, lxp)) ltype

let ctx_define_rec (ctx: elab_context) decls =
  let nctx = ectx_extend_rec ctx decls in
  let _ = List.fold_left (fun n (var, lxp, ltp)
                          -> elab_check_proper_type
                              nctx (push_susp ltp (S.shift n)) var;
                            n - 1)
                         (List.length decls)
                         decls in
  let _ = List.fold_left (fun n (var, lxp, ltp)
                          -> elab_check_def nctx var lxp
                                           (push_susp ltp (S.shift n));
                            n - 1)
                         (List.length decls)
                         decls in
  nctx

(*  The main job of lexp (currently) is to determine variable name (index)
 *  and to regroup type specification with their variable
 *
 *  elab_context is composed of two environment: senv and env.
 *  the senv environment is used to find the correct debruijn index
 *  while the env environment is used to save variable information.
 *  the env environment look a lot like the runtime environment that will be
 *  used in the eval section.
 *
 *  While most of the time senv and env will be synchronised it is
 *  possible for env to hold more variables than senv since senv is a map
 *  which does not allow multiple definition while env does.
 *
 *)

(*
 *      Type Inference
 * --------------------- *)
(* Parsing a Pexp into an Lexp is really "elaboration", i.e. it needs to
 * infer the types and perform macro-expansion.
 *
 * More specifically, we do it with 2 mutually recursive functions:
 * - `check` takes an Sexp along with its expected type and returns an Lexp
 *   of that type (hopefully)
 * - `infer` takes an Sexp and infers its type (which it returns along with
 *   the Lexp).
 * This is the idea of "bidirectional type checking", which minimizes
 * the amount of "guessing" and/or annotations.  Since we infer types anyway
 * it doesn't really reduce the amount of type annotations for us, but it
 * reduces the amount of inference and checking, i.e. it reduces the number of
 * metavars we create/instantiate/dereference as well as the number of call to
 * the unification algorithm.
 * Basically guessing/annotations is only needed at those few places where the
 * code is not fully-normalized (which in normal programs is only in "let"
 * definitions) as well as when we fill implicit arguments.
 *)

let newMetavar (ctx : lexp_context) sl name t =
  let meta = Unif.create_metavar ctx sl t in
  mkMetavar (meta, S.identity, name)

let newMetalevel (ctx : lexp_context) sl loc =
  newMetavar ctx sl (loc, Some "ℓ") type_level

let newMetatype (ctx : lexp_context) sl loc
  = newMetavar ctx sl (loc, Some "τ")
               (mkSort (loc, Stype (newMetalevel ctx sl loc)))

(* Functions used when we need to return some lexp/ltype but
 * an error makes it impossible to return "the right one".  *)
let mkDummy_type ctx loc = newMetatype (ectx_to_lctx ctx) dummy_scope_level loc
let mkDummy_check ctx loc t = newMetavar (ectx_to_lctx ctx) dummy_scope_level
                                         (loc, None) t
let mkDummy_infer ctx loc =
  let t = newMetatype (ectx_to_lctx ctx) dummy_scope_level loc in
  (mkDummy_check ctx loc t, t)

let sdform_define_operator (ctx : elab_context) loc sargs _ot : elab_context =
  match sargs with
  | [String (_, name); l; r]
    -> let level s = match s with
        | Symbol (_, "") -> None
        | Integer (_, n) -> Some (Z.to_int n)
        | _ -> sexp_error (sexp_location s) "Expecting an integer or ()"; None in
      let (grm, a, b, c) = ctx in
      (SMap.add name (level l, level r) grm, a, b, c)
  | [o; _; _]
    -> sexp_error (sexp_location o) "Expecting a string"; ctx
  | _
    -> sexp_error loc "define-operator expects 3 argument"; ctx

let sform_dummy_ret ctx loc =
  let t = newMetatype (ectx_to_lctx ctx) dummy_scope_level loc in
  (newMetavar (ectx_to_lctx ctx) dummy_scope_level
     (loc, Some "special-form-error") t,
   Inferred t)

let elab_varref ctx (loc, name)
  = try
      let idx = senv_lookup name ctx in
      let id = (loc, Some name) in
      let lxp = mkVar (id, idx) in
      let ltp = env_lookup_type ctx (id, idx) in
      (lxp, Inferred ltp)
  with Senv_Lookup_Fail xs ->
    (let relateds =
       if ((List.length xs) > 0) then
         ". Did you mean: " ^ (String.concat " or " xs) ^" ?"
       else "" in
     sexp_error loc ("The variable: `" ^ name ^
                       "` was not declared" ^ relateds);
     sform_dummy_ret ctx loc)

(* Turn metavar into plain vars after generalization.
 * ids: an IMap that maps metavars to their position as argument
 *   (first arg gets position 0).  *)
let rec meta_to_var ids (e : lexp) =

  let count = IMap.cardinal ids in

  (* Yuck!  Yuck!  Yuck!
   * This is very messy.  What we need to do starts as follows:
   * We have a `Γ ⊢ e : τ` and this `e` contains some metavars `m₁…mₙ : τ₁…τₙ`
   * that we want to change into formal arguments, i.e. we want to turn `e`
   * into something like
   *
   *     Γ ⊢ λ x₁…xₙ ≡> e[x₁…xₙ/m₁…mₙ] : τ₁…τₙ ≡> τ
   *
   * The above substitution is not the usual capture-avoiding substitution
   * since it replaces metavars with vars rather than vars with terms.
   * It's more like *instanciation* of those metavars.
   * And indeed, ideally it should be a simple matter of instanciating
   * those metavars with temrs that are variable references.
   *
   * An important aspect here is that the rest of `e` also needs to be
   * changed because it will now live in a new context:
   *
   *     Γ,x₁:τ₁,…,xₙ:τₙ ⊢ e[x₁…xₙ/m₁…mₙ] : τ
   *
   * So we need to adjust all the variable references in `e` to account for
   * that, which we normally do with a simple `S.shift n`.
   *
   * The first problem comes here: `S.shift n` takes a term from
   * `Γ` to `Γ,x₁:τ₁,…,xₙ:τₙ`, making sure it still references the same
   * bindings as before, i.e. it makes sure the result *cannot* refer to
   * any `x₁…xₙ`!
   * I first thought "it's OK, I'll first do the `S.shift n` and I only
   * instanciate the metavars afterwards", but that does not help,
   * because every reference to a metavar is properly wrapped in a
   * pending application of the relevant substitution, so the `S.shift n`
   * still applies to it.
   *
   * For this reason, we have a `loop` below which does the substitution
   * of variable references for the metavars (since just instanciating
   * the metavars doesn't work).
   *
   * The second problem comes with the other metavars in `e`.
   * There are 3 kinds of metavars in `e`:
   *
   * A. Those that we want to replace with variable references.
   * B. Those that live in some higher enclosing scope.
   * C. The "others".
   *
   * Presumably, we have (A) under control.
   * For (B) the situation is easy enough: since they live in a higher
   * enlosing scope they can only refer to those variables that exist in
   * some prefix of `Γ`, so we just need to apply `S.shift n` to them.
   *
   * For (C), we'd like those metavars (which originally could refer to
   * any var in `Γ`) to now be a able to also refer to any of the
   * new vars `x₁…xₙ`.  So `S.shift n` is definitely not right for them.
   * Instead, I ended up writing `adjust_subst` which hacks up
   * the substitution attached to each metavar reference so it can
   * now refer to its original context *and* to `x₁…xₙ`.
   *
   * Arguably, now that we handle (C), we could handle (A) by
   * first treating them as (C) and then using instantiation.
   * But the code for (A) was already written, and it doesn't seem like
   * it would make things any simpler currently.
   *
   * Note: in the original HM algorithm (and in Twelf), (C) cannot occur
   * because every metavariable that's not in (B) is generalized
   * (i.e. will be in A).  But here we need (C) for cases like:
   *
   *     List : Type ? -> Type ?;
   *
   * Since the second `?` should not be generalized but should
   * be inferred from the definition.  I.e. the above should be
   * generalized to:
   *
   *     List : (ℓ : TypeLevel) ≡> Type ℓ -> Type ?;
   *
   * where the remaining `?` will be inferred by unification
   * when type-checking the definition of `Box` where ti be unified with `ℓ`,
   * the newly introduced variable!
   *)

  (* `o` is the binding offset until the root.  *)
  let rec adjust_subst o s = match s with
    | S.Identity n
      -> let o' = o - n in
        if o' < 0 then
          (* This metavar's original context is outside of our scope
           * (case (B) above), so don't let it refer to the new vars. *)
          S.Identity (n + count)
        else
          S.Identity n
    | S.Cons (e, s', n)
      -> let o' = o - n in
        if o' < 0 then
          (* This metavar's original context is outside of our scope
           * (case (B) above), so don't let it refer to the new vars. *)
          S.Cons (e, s', n + count)
        else
          S.Cons (loop o' e, adjust_subst o' s', n)

  (* `o` is the binding depth at which we are relative to the "root"
   * of the expression (i.e. where the new vars will be inserted).  *)
  and loop o e =
    match lexp_lexp' e with
    | Imm _ -> e
    | SortLevel SLz -> e
    | SortLevel (SLsucc e) -> mkSortLevel (mkSLsucc (loop o e))
    | SortLevel (SLlub (e1, e2)) -> mkSortLevel (mkSLlub' (loop o e1, loop o e2))
    | Sort (l, Stype e) -> mkSort (l, Stype (loop o e))
    | Sort (_, (StypeOmega | StypeLevel)) -> e
    | Builtin _ -> e
    | Var (n,i) -> if i < o then e else mkVar (n, i + count)
    | Susp (e, s) -> loop o (push_susp e s)
    | Let (l, defs, e)
      -> let len = List.length defs in
        let (_, ndefs)
          = List.fold_right (fun (l,e,t) (o', defs)
                             -> let o' = o' - 1 in
                               (o', (l, loop (len + o) e,
                                     loop (o' + o) t) :: defs))
                            defs (len, []) in
        mkLet (l, ndefs, loop (len + o) e)
    | Arrow (ak, v, t1, l, t2)
      -> mkArrow (ak, v, loop o t1, l, loop (1 + o) t2)
    | Lambda (ak, v, t, e)
      -> mkLambda (ak, v, loop o t, loop (1 + o) e)
    | Call (f, args)
      -> mkCall (loop o f, List.map (fun (ak, e) -> (ak, loop o e)) args)
    | Inductive (l, label, args, cases)
      -> let alen = List.length args in
        let (_, nargs)
          = List.fold_right (fun (ak, v, t) (o', args)
                             -> let o' = o' - 1 in
                               (o', (ak, v, loop (o' + o) t)
                                    :: args))
                            args (alen, []) in
        let ncases
          = SMap.map
              (fun fields
               -> let flen = List.length fields in
                 let (_, nfields)
                   = List.fold_right
                       (fun (ak, v, t) (o', fields)
                        -> let o' = o' - 1 in
                          (o', (ak, v, loop (o' + o) t)
                               :: fields))
                       fields (flen + alen, []) in
                 nfields)
              cases in
        mkInductive (l, label, nargs, ncases)
    | Cons (t, l) -> mkCons (loop o t, l)
    | Case (l, e, t, cases, default)
      -> let ncases
          = SMap.map
              (fun (l, fields, e)
               -> (l, fields, loop (o + List.length fields + 1) e))
              cases in
        mkCase (l, loop o e, loop o t, ncases,
                match default with None -> None
                                 | Some (v, e) -> Some (v, loop (2 + o) e))
    | Metavar (id, s, name)
      -> if IMap.mem id ids then
          mkVar (name, o + count - IMap.find id ids)
        else match metavar_lookup id with
             | MVal e -> loop o (push_susp e s)
             | _ -> mkMetavar (id, adjust_subst o s, name)
  in loop 0 e

let sort_generalized_metavars sl cl ctx mfvs =
  (* TypeLevel parameters have to come first, and the metavariables
     must be sorted topologically, so that the type of a parameter can
     only refer to earlier parameters.

     We do a DFS through the metavariable types to sort them and we
     store any TypeLevel argument in a separate list. *)
  let rec handle_metavar id (sl', mt, cl', vname) (* The metavar *)
            ((typelevels, (* The front of the list (in reverse) *)
              params, (* The rest of the list (in reverse) *)
              seen) (* The set of indices already added to the lists *)
             as acc) =
    if sl' < sl || (* This metavar appears in the context, so we can't
                      generalize over it.  *)
         (IMap.mem id seen) (* We've already handled this metavar, skip it. *)
    then acc else
      (assert (cl' >= cl);
       let mt = if cl' > cl then
                  Inverse_subst.apply_inv_subst mt (S.shift (cl' - cl))
                else mt in
       let mv = (id, vname, mt) in
       if (OL.conv_p ctx mt type_level) then
         ((mv :: typelevels), params, (IMap.add id () seen))
       else
         let (_, (mt_mfvs, _)) = OL.fv mt in
         let (typelevels, params, seen) = handle_metavars mt_mfvs acc in
         (typelevels, mv :: params, (IMap.add id () seen))
         )
    and handle_metavars ids acc = IMap.fold handle_metavar ids acc in
  let (typelevels, params, _) = handle_metavars mfvs ([], [], IMap.empty) in
  List.append (List.rev typelevels) (List.rev params)

(* Generalize expression `e` with respect to its uninstantiated metavars.
 * `wrap` is the function that adds the relevant quantification, typically
 * either mkArrow or mkLambda.  *)
let generalize (nctx : elab_context) e =
  let l = lexp_location e in
  let sl = ectx_to_scope_level nctx in
  let cl = Myers.length (ectx_to_lctx nctx) in
  let (_, (mfvs, nes)) = OL.fv e in
  if mfvs = IMap.empty
  then (fun wrap e -> e) (* Nothing to generalize, yay!  *)
  else
    (* Sort `mvfs' topologically, and bring typelevel args to the front *)
    let mfvs = sort_generalized_metavars sl cl (ectx_to_lctx nctx) mfvs in
    let len = List.length mfvs in
    fun wrap e ->
    let rec loop ids n mfvs =
      assert (n = IMap.cardinal ids);
      match mfvs with
      | [] -> assert (n = len);
             meta_to_var ids e
      | ((id, vname, mt) :: mfvs)
        -> let mt' = meta_to_var ids mt in
          let n = n + 1 in
          let e' = loop (IMap.add id n ids) n mfvs in
          wrap (IMap.mem id nes) vname mt' l e' in
    loop (IMap.empty) 0 mfvs

let elab_p_id ((l,name) : symbol) : vname =
  (l, match name with "_" -> None | _ -> Some name)

(* Infer or check, as the case may be.  *)
let rec elaborate ctx se ot =
  match se with
  (* Rewrite SYM to `typer-identifier SYM`.  *)
  | Symbol ((loc, name) as id)
    -> if not (name = "typer-identifier") then
        elaborate ctx (Node (Symbol (loc, "typer-identifier"), [se])) ot
      (* Break the inf-recursion!  *)
      else elab_varref ctx id

  (* Rewrite IMM to `typer-immediate IMM`.  *)
  | (Integer _ | Float _ | String _ | Block _)
    -> let l = sexp_location se in
      elaborate ctx (Node (Symbol (l, "typer-immediate"), [se])) ot

  | Node (se, []) -> elaborate ctx se ot

  | Node (func, args)
    -> let (f, t) as ft = infer func ctx in
      if (OL.conv_p (ectx_to_lctx ctx) t type_special_form) then
        elab_special_form ctx f args ot
      else if (OL.conv_p (ectx_to_lctx ctx) t
                         (BI.get_predef "Macro" ctx)) then
        elab_macro_call ctx f args ot
      else
        (* FIXME: I'd like to do something like:
         *    elaborate ctx (Node (Symbol (l, "typer-funcall"), func::args)) ot
         * but that forces `typer-funcall` to elaborate `func` a second time!
         * Maybe I should only elaborate `func` above if it's a symbol
         * (and maybe even use `elab_varref` rather than indirecting
         * through `typer-identifier`)?  *)
        elab_call ctx ft args

and infer (p : sexp) (ctx : elab_context): lexp * ltype =
  match elaborate ctx p None with
  | (_, Checked) -> fatal ~loc:(sexp_location p) "`infer` got Checked!"
  | (e, Lazy) -> (e, OL.get_type (ectx_to_lctx ctx) e)
  | (e, Inferred t) -> (e, t)

and elab_special_form ctx f args ot =
  let loc = lexp_location f in
  match (OL.lexp'_whnf f (ectx_to_lctx ctx)) with
  | Builtin ((_, name), _) ->
     (* Special form.  *)
     (get_special_form name) ctx loc args ot

  | _ -> lexp_error loc f ("Unknown special-form: " ^ lexp_string f);
        sform_dummy_ret ctx loc

(* Make up an argument of type `t` when none is provided.  *)
and get_implicit_arg ctx loc oname t =
  newMetavar
    (ectx_to_lctx ctx)
    (ectx_to_scope_level ctx)
    (loc, oname)
    t

(* Build the list of implicit arguments to instantiate.  *)
and instantiate_implicit e t ctx =
  let rec instantiate t args =
    match OL.lexp'_whnf t (ectx_to_lctx ctx) with
    | Arrow ((Aerasable | Aimplicit) as ak, (_, v), t1, _, t2)
      -> let arg = get_implicit_arg ctx (lexp_location e) v t1 in
        instantiate (mkSusp t2 (S.substitute arg)) ((ak, arg)::args)
    | _ -> (mkCall (e, List.rev args), t)
  in instantiate t []

and infer_type pexp ectx var =
  (* We could also use lexp_check with an argument of the form
   * Sort (?s), but in most cases the metavar would be allocated
   * unnecessarily.  *)
  let t, s = infer pexp ectx in
  (match OL.lexp'_whnf s (ectx_to_lctx ectx) with
   | Sort (_, _) -> () (* All clear!  *)
   (* FIXME: We could automatically coerce Type levels to Sorts, so we
    * could write `(a : TypeLevel) -> a -> a` instead of
    * `(a : TypeLevel) -> Type_ a -> Type_ a`  *)
   | _ ->
      (* FIXME: Here we rule out TypeLevel/TypeOmega.
       * Maybe it's actually correct?!  *)
      let l = lexp_location s in
      match
        Unif.unify (mkSort (l, Stype (newMetalevel
                                        (ectx_to_lctx ectx)
                                        (ectx_to_scope_level ectx)
                                        l)))
                   s
                   (ectx_to_lctx ectx) with
      | (_::_)
        -> (let typestr = lexp_string t ^ " : " ^ lexp_string s in
           match var with
           | (l, None) -> lexp_error l t
                           ("`" ^ typestr ^ "` is not a proper type")
           | (l, Some name)
             -> lexp_error l t
                          ("Type of `" ^ name ^ "` is not a proper type: "
                           ^ typestr))
      | [] -> ());
  t

and lexp_let_decls declss (body: lexp) ctx =
  List.fold_right (fun decls lxp -> mkLet (dloc, decls, lxp))
                  declss body

and unify_with_arrow ctx tloc lxp kind var aty
  = let arg = match aty with
      | None -> newMetatype (ectx_to_lctx ctx) (ectx_to_scope_level ctx) tloc
      | Some laty -> laty in
    let nctx = ectx_extend ctx var Variable arg in
    let body = newMetatype (ectx_to_lctx nctx) (ectx_to_scope_level ctx) tloc in
    let (l, _) = var in
    let arrow = mkArrow (kind, var, arg, l, body) in
    match Unif.unify arrow lxp (ectx_to_lctx ctx) with
    | ((_ck, _ctx, t1, t2)::_)
      -> lexp_error tloc lxp ("Types:\n    " ^ lexp_string t1
                             ^ "\n and:\n    "
                             ^ lexp_string t2
                             ^ "\n do not match!");
        (mkDummy_type ctx l, mkDummy_type nctx l)
    | [] -> arg, body

and check (p : sexp) (t : ltype) (ctx : elab_context): lexp =
  let (e, ot) = elaborate ctx p (Some t) in
  match ot with
  | Checked -> e
  | _ -> let inferred_t = match ot with Inferred t -> t
                                      | _ -> OL.get_type (ectx_to_lctx ctx) e in
        check_inferred ctx e inferred_t t

and unify_or_error lctx lxp ?lxp_name expect actual =
  match Unif.unify expect actual lctx with
   | ((ck, _ctx, t1, t2)::_)
     -> lexp_error (lexp_location lxp) lxp
         ("Type mismatch"
          ^ (match ck with | Unif.CKimpossible -> ""
                           | Unif.CKresidual -> " (residue)")
          ^ "!  Context expected:\n    " ^ lexp_string expect ^ "\nbut "
          ^ (U.option_default "expression" lxp_name) ^ " has type:\n    "
          ^ lexp_string actual ^ "\ncan't unify:\n    "
          ^ lexp_string t1
          ^ "\nwith:\n    "
          ^ lexp_string t2);
       assert (not (OL.conv_p lctx expect actual))
   | [] -> ()

(* This is a crucial function: take an expression `e` of type `inferred_t`
 * and convert it into something of type `t`.  Currently the only conversion
 * we use is to instantiate implicit arguments when needed, but we could/should
 * do lots of other things.  *)
and check_inferred ctx e inferred_t t =
  let (e, inferred_t) =
  match OL.lexp'_whnf t (ectx_to_lctx ctx) with
    | Arrow ((Aerasable | Aimplicit), _, _, _, _)
      -> (e, inferred_t)
    | _ -> instantiate_implicit e inferred_t ctx in
  unify_or_error (ectx_to_lctx ctx) e t inferred_t;
  e

(* Lexp.case can sometimes be inferred, but we prefer to always check.  *)
and check_case rtype (loc, target, ppatterns) ctx =
  (* Helpers *)

  let pat_string p = sexp_string (pexp_u_pat p) in

    let uniqueness_warn pat =
      warning ~loc:(pexp_pat_location pat)
              ("Pattern " ^ pat_string pat
               ^ " is a duplicate.  It will override previous pattern.") in

    let check_uniqueness pat name map =
      if SMap.mem name map then uniqueness_warn pat in

    (* get target and its type *)
    let tlxp, tltp = infer target ctx in
    let tknd = OL.get_type (ectx_to_lctx ctx) tltp in
    let tlvl = match OL.lexp'_whnf tknd (ectx_to_lctx ctx) with
      | Sort (_, Stype l) -> l
      | _ -> fatal "Target lexp's kind is not a sort" in
    let it_cs_as = ref None in
    let ltarget = ref tlxp in

    let get_cs_as it' lctor =
      let unify_ind expected actual =
        match Unif.unify actual expected (ectx_to_lctx ctx) with
        | (_::_)
          -> lexp_error loc lctor
               ("Expected pattern of type `" ^ lexp_string expected
                ^ "` but got `" ^ lexp_string actual ^ "`")
        | [] -> () in
      match !it_cs_as with
      | Some (it, cs, args)
        -> unify_ind it it'; (cs, args)
      | None
        -> match OL.lexp'_whnf it' (ectx_to_lctx ctx) with
          | Inductive (_, _, fargs, constructors)
            -> let (s, targs) = List.fold_left
                                 (fun (s, targs) (ak, name, t)
                                  -> let arg = newMetavar
                                                (ectx_to_lctx ctx)
                                                (ectx_to_scope_level ctx)
                                                name (mkSusp t s) in
                                    (S.cons arg s, (ak, arg) :: targs))
                                 (S.identity, [])
                                 fargs in
              let (cs, args) = (constructors, List.rev targs) in
              ltarget := check_inferred ctx tlxp tltp (mkCall (it', args));
              it_cs_as := Some (it', cs, args);
              (cs, args)
          | _ -> let call_split e =
                      match OL.lexp'_whnf e (ectx_to_lctx ctx) with
                       | Call (f, args) -> (f, args)
                       | _ -> (e,[]) in
                let (it, targs) = call_split tltp in
                unify_ind it it';
                let constructors =
                  match OL.lexp'_whnf it (ectx_to_lctx ctx) with
                  | Inductive (_, _, fargs, constructors)
                    -> assert (List.length fargs = List.length targs);
                      constructors
                  | _ -> lexp_error (sexp_location target) tlxp
                          ("Can't `case` on objects of this type: "
                           ^ lexp_string tltp);
                        SMap.empty in
                it_cs_as := Some (it, constructors, targs);
                (constructors, targs) in

    (*  Read patterns one by one *)
    let fold_fun (lbranches, dflt) (pat, pexp) =

      let shift_to_extended_ctx nctx lexp =
        mkSusp lexp (S.shift (M.length (ectx_to_lctx nctx)
                              - M.length (ectx_to_lctx ctx))) in

      let ctx_extend_with_eq nctx head_lexp =
        (* Add a proof of equality between the target and the branch
           head to the context *)
        let tlxp' = shift_to_extended_ctx nctx tlxp in
        let tltp' = shift_to_extended_ctx nctx tltp in
        let tlvl' = shift_to_extended_ctx nctx tlvl in
        let eqty = mkCall (DB.type_eq,
                           [(Aerasable, tlvl');    (* Typelevel *)
                            (Aerasable, tltp');    (* Inductive type *)
                            (Anormal, head_lexp);  (* Lexp of the branch head *)
                            (Anormal, tlxp')])     (* Target lexp *)
        in ctx_extend nctx (loc, None) Variable eqty
      in

      let add_default v =
        (if dflt != None then uniqueness_warn pat);
        let nctx = ctx_extend ctx v Variable tltp in
        let head_lexp = mkVar (v, 0) in
        let nctx = ctx_extend_with_eq nctx head_lexp in
        let rtype' = shift_to_extended_ctx nctx rtype in
        let lexp = check pexp rtype' nctx in
        lbranches, Some (v, lexp) in

      let add_branch pctor pargs =
        let loc = sexp_location pctor in
        let lctor, ct = infer pctor ctx in
        let rec inst_args ctx e =
        let lxp = OL.lexp_whnf e (ectx_to_lctx ctx) in
         match lexp_lexp' lxp with
          | Lambda (Aerasable, v, t, body)
            -> let arg = newMetavar (ectx_to_lctx ctx) (ectx_to_scope_level ctx)
                          v t in
              let nctx = ctx_extend ctx v Variable t in
              let body = inst_args nctx body in
              mkSusp body (S.substitute arg)
          | e -> lxp in
        match lexp_lexp' (nosusp (inst_args ctx lctor)) with
        | Cons (it', (_, cons_name))
          -> let _ = check_uniqueness pat cons_name lbranches in
            let (constructors, targs) = get_cs_as it' lctor in
            let cargs
              = try SMap.find cons_name constructors
                with Not_found
                     -> lexp_error loc lctor
                                  ("`" ^ (lexp_string it')
                                   ^ "` does not have a `"
                                   ^ cons_name ^ "` constructor");
                       [] in

            let subst = List.fold_left (fun s (_, t) -> S.cons t s)
                                        S.identity targs in
            let rec make_nctx ctx   (* elab context.  *)
                              s     (* Pending substitution.  *)
                              pargs (* Pattern arguments.  *)
                              cargs (* Constructor arguments.  *)
                              pe    (* Pending explicit pattern args.  *)
                              acc = (* Accumulated result.  *)
              match (pargs, cargs) with
              | (_, []) when not (SMap.is_empty pe)
                -> let pending = SMap.bindings pe in
                  sexp_error loc
                             ("Explicit pattern args `"
                              ^ String.concat ", " (List.map (fun (l, _) -> l)
                                                             pending)
                              ^ "` have no matching fields");
                  make_nctx ctx s pargs cargs SMap.empty acc
              | [], [] -> ctx, List.rev acc
              | (_, pat)::_, []
                -> lexp_error loc lctor
                             "Too many pattern args to the constructor";
                  make_nctx ctx s [] [] pe acc
              | (_, (ak, (_, Some fname), fty)::cargs)
                   when SMap.mem fname pe
                -> let var = SMap.find fname pe in
                  let nctx = ctx_extend ctx var Variable (mkSusp fty s) in
                  make_nctx nctx (ssink var s) pargs cargs
                            (SMap.remove fname pe)
                            ((ak, var)::acc)
              | ((ef, var)::pargs, (ak, _, fty)::cargs)
                   when (match (ef, ak) with
                         | (Some (_, "_"), _) | (None, Anormal) -> true
                         | _ -> false)
                -> let nctx = ctx_extend ctx var Variable (mkSusp fty s) in
                  make_nctx nctx (ssink var s) pargs cargs pe
                            ((ak, var)::acc)
              | ((Some (l, fname), var)::pargs, cargs)
                -> if SMap.mem fname pe then
                    sexp_error l ("Duplicate explicit field `" ^ fname ^ "`");
                  make_nctx ctx s pargs cargs (SMap.add fname var pe) acc
              | pargs, (ak, fname, fty)::cargs
                -> let var = (loc, None) in
                  let nctx = ctx_extend ctx var Variable (mkSusp fty s) in
                  if ak = Anormal then
                    sexp_error loc
                               ("Missing pattern for normal field"
                                ^ (match fname with (_, Some n) -> " `" ^ n ^ "`"
                                                  | _ -> ""));
                  make_nctx nctx (ssink var s) pargs cargs pe
                            ((ak, var)::acc) in
            let nctx, fargs = make_nctx ctx subst pargs cargs SMap.empty [] in
            let head_lexp_ctor =
              shift_to_extended_ctx nctx
                (mkCall (lctor, List.map (fun (_, a) -> (Aerasable, a)) targs)) in
            let head_lexp_args =
              List.mapi (fun i (ak, vname) ->
                  (ak, mkVar (vname, List.length fargs - i - 1))) fargs in
            let head_lexp = mkCall (head_lexp_ctor, head_lexp_args) in
            let nctx = ctx_extend_with_eq nctx head_lexp in
            let rtype' = shift_to_extended_ctx nctx rtype in
            let lexp = check pexp rtype' nctx in
            SMap.add cons_name (loc, fargs, lexp) lbranches,
            dflt
        (* FIXME: If `ct` is Special-Form or Macro, pass pargs to it
         * and try again with the result.  *)
        | _ -> lexp_error loc lctor "Not a constructor"; lbranches, dflt
      in

      match pat with
      | Ppatsym ((_, None) as var) -> add_default var
      | Ppatsym ((l, Some name) as var)
        -> if Eval.constructor_p name ctx then
            add_branch (Symbol (l, name)) []
          else add_default var (* A named default branch.  *)

      | Ppatcons (pctor, pargs) -> add_branch pctor pargs in

    let (lpattern, dflt) =
        List.fold_left fold_fun (SMap.empty, None) ppatterns in

    mkCase (loc, tlxp, rtype, lpattern, dflt)

and elab_macro_call ctx func args ot =
  let t
    = match ot with
    | None -> newMetatype (ectx_to_lctx ctx) (ectx_to_scope_level ctx)
                         (lexp_location func)
    | Some t -> t in
  let sxp = match lexp_expand_macro (lexp_location func)
                                    func args ctx (Some t) with
    | Vcommand cmd
      -> (match cmd () with
         | Vsexp (sxp) -> sxp
         | v -> value_fatal (lexp_location func) v
                 "Macros should return a IO Sexp")
    | v -> value_fatal (lexp_location func) v
                      "Macros should return an IO" in
  elaborate ctx sxp ot

(*  Identify Call Type and return processed call.  *)
and elab_call ctx (func, ltp) (sargs: sexp list) =
  let loc = lexp_location func in

  let rec handle_fun_args largs sargs pending ltp =
    let ltp' = OL.lexp_whnf ltp (ectx_to_lctx ctx) in
    match sargs, lexp_lexp' ltp' with
    | _, Arrow (ak, (_, Some aname), arg_type, _, ret_type)
         when SMap.mem aname pending
      -> let sarg = SMap.find aname pending in
        let larg = check sarg arg_type ctx in
        handle_fun_args ((ak, larg) :: largs) sargs
                        (SMap.remove aname pending)
                        (L.mkSusp ret_type (S.substitute larg))

    | (Node (Symbol (_, "_:=_"), [Symbol (_, aname); sarg])) :: sargs,
      Arrow (ak, _, arg_type, _, ret_type)
         when (aname = "_")
                (* Explicit-implicit argument.  *)
      -> let larg = check sarg arg_type ctx in
        handle_fun_args ((ak, larg) :: largs) sargs pending
                        (L.mkSusp ret_type (S.substitute larg))

    | (Node (Symbol (_, "_:=_"), [Symbol (l, aname); sarg])) :: sargs,
      Arrow _
      -> if SMap.mem aname pending then
          sexp_error l ("Duplicate explicit arg `" ^ aname ^ "`");
        handle_fun_args largs sargs (SMap.add aname sarg pending) ltp

    | (Node (Symbol (_, "_:=_"), Symbol (l, aname) :: _)) :: sargs, _
      -> sexp_error l
                   ("Explicit arg `" ^ aname ^ "` to non-function "
                    ^ "(type = " ^ (lexp_string ltp) ^ ")");
        handle_fun_args largs sargs pending ltp

    (* Aerasable *)
    | _, Arrow ((Aerasable | Aimplicit) as ak, (l,v), arg_type, _, ret_type)
         (* Don't instantiate after the last explicit arg: the rest is done,
          * when needed in infer_and_check (via instantiate_implicit).  *)
         when not (sargs = [] && SMap.is_empty pending)
      -> let larg = get_implicit_arg
                     ctx (match sargs with
                          | [] -> loc
                          | sarg::_ -> sexp_location sarg)
                     v arg_type in
        handle_fun_args ((ak, larg) :: largs) sargs pending
                        (L.mkSusp ret_type (S.substitute larg))
    | [], _
      -> (if not (SMap.is_empty pending) then
           let pending = SMap.bindings pending in
           let loc = match pending with
             | (_, sarg)::_ -> sexp_location sarg
             | _ -> assert false in
           lexp_error loc func
                      ("Explicit actual args `"
                       ^ String.concat ", " (List.map (fun (l, _) -> l)
                                                      pending)
                       ^ "` have no matching formal args"));
        largs, ltp

    | sarg :: sargs, _
      -> let (arg_type, ret_type) = match lexp_lexp' ltp' with
          | Arrow (ak, _, arg_type, _, ret_type)
            -> assert (ak = Anormal); (arg_type, ret_type)
          | _ -> unify_with_arrow ctx (sexp_location sarg)
                                 ltp' Anormal (dloc, None) None in
        let larg = check sarg arg_type ctx in
        handle_fun_args ((Anormal, larg) :: largs) sargs pending
                        (L.mkSusp ret_type (S.substitute larg)) in

  let (largs, ret_type) = handle_fun_args [] sargs SMap.empty ltp in
  (mkCall (func, List.rev largs), Inferred ret_type)

(*  Parse inductive type definition.  *)
and lexp_parse_inductive ctors ctx =

  let make_args (args:(arg_kind * vname * sexp) list) ctx
      : (arg_kind * vname * ltype) list =
    let nctx = ectx_new_scope ctx in
    let rec loop args acc ctx =
          match args with
          | [] -> let acc = List.rev acc in
                 (* Convert the list of fields into a Lexp expression.
                  * The actual expression doesn't matter, as long as its
                  * scoping is right: we only use it so we can pass it to
                  * things like `fv` and `meta_to_var`.  *)
                 let altacc = List.fold_right
                                (fun (ak, n, t) aa
                                 -> mkArrow (ak, n, t, dummy_location, aa))
                                acc impossible in
                 let g = generalize nctx altacc in
                 let altacc' = g (fun _ne vname t l e
                                  -> mkArrow (Aerasable, vname, t, l, e))
                                 altacc in
                 if altacc' == altacc
                 then acc       (* No generalization!  *)
                 else
                   (* Convert the Lexp back into a list of fields.  *)
                   let rec loop e =
                    match lexp_lexp' e with
                     | Arrow (ak, n, t, _, e) -> (ak, n, t)::(loop e)
                     | _ -> assert (e = impossible); [] in
                   loop altacc'
          | (kind, var, exp)::tl
            -> let lxp = infer_type exp ctx var in
              let nctx = ectx_extend ctx var Variable lxp in
              loop tl ((kind, var, lxp)::acc) nctx in
    loop args [] nctx in

  List.fold_left
    (fun lctors ((_, name), args) ->
      SMap.add name (make_args args ctx) lctors)
    SMap.empty ctors

and track_fv rctx lctx e =
  let (fvs, (mvs, _)) = OL.fv e in
  let nc = EV.not_closed rctx fvs in
  if nc = [] && not (IMap.is_empty mvs) then
    "metavars"
  else if nc = [] then
    "a bug"
  else let tfv i =
         let name = match Myers.nth i rctx with
           | ((_, Some n),_) -> n
           | _ -> "<anon>" in
         match Myers.nth i lctx with
         | (_, LetDef (o, e), _)
           -> let drop = i + 1 - o in
             if drop <= 0 then
               "somevars[" ^ string_of_int i ^ "-" ^ string_of_int o ^ "]"
             else
               name ^ " ("
               ^ track_fv (Myers.nthcdr drop rctx)
                          (Myers.nthcdr drop lctx)
                          e
               ^ ")"
         | _ -> name
       in String.concat " " (List.map tfv nc)

and lexp_eval ectx e =
  let ee = OL.erase_type e in
  let rctx = EV.from_ectx ectx in

  if not (EV.closed_p rctx (OL.fv e)) then
    lexp_error (lexp_location e) e
               ("Expression `" ^ lexp_string e ^ "` is not closed: "
                ^ track_fv rctx (ectx_to_lctx ectx) e);

  try EV.eval ee rctx
  with exc ->
    let eval_trace = fst (EV.get_trace ()) in
    info ~print_action:(fun _ -> EV.print_eval_trace (Some eval_trace))
      "Exception happened during evaluation:";
    raise exc

and lexp_expand_macro loc macro_funct sargs ctx (ot : ltype option)
    : value_type =

  (* Build the function to be called *)
  let macro_expand = BI.get_predef "Macro_expand" ctx in
  (* FIXME: Rather than remember the lexp of "expand_macro" in predef,
   * we should remember its value so we don't have to re-eval it everytime.  *)
  let macro_expand = lexp_eval ctx macro_expand in
  (* FIXME: provide `ot` (the optional expected type) for non-decl macros.  *)
  let macro = lexp_eval ctx macro_funct in
  let args = [macro; BI.o2v_list sargs] in

  (* FIXME: Make a proper `Var`.  *)
  EV.eval_call loc (EL.Var ((DB.dloc, Some "expand_macro"), 0)) ([], [])
               macro_expand args

(* Print each generated decls *)
(* and sexp_decls_macro_print sxp_decls =
 *   match sxp_decls with
 *     | Node (Symbol (_, "_;_"), decls) ->
 *       List.iter (fun sxp -> sexp_decls_macro_print sxp) decls
 *     | e -> sexp_print e; print_string "\n" *)

and lexp_decls_macro (loc, mname) sargs ctx: sexp =
  try let lxp, ltp = infer (Symbol (loc, mname)) ctx in

      (* FIXME: Check that (conv_p ltp Macro)!  *)
      let ret = lexp_expand_macro loc lxp sargs ctx None in
      match ret with
      | Vcommand cmd
        -> (match (cmd ()) with
           | Vsexp (sexp) -> sexp
           | _ -> fatal ~loc ("Macro `" ^ mname ^ "` should return a IO sexp"))
      | _ -> fatal ~loc ("Macro `" ^ mname ^ "` should return an IO")

  with e ->
    fatal ~loc ("Macro `" ^ mname ^ "` not found")

(* Elaborate a bunch of mutually-recursive definitions.
 * FIXME: Currently, we never apply generalization to recursive definitions,
 * which can leave "bogus" metavariables in the term (and  it also means we
 * can't define `List_length` or `List_map` without adding explicit type
 * annotations :-( ).  *)
and lexp_check_decls (ectx : elab_context) (* External context.  *)
                     (nctx : elab_context) (* Context with type declarations. *)
                     (defs : (symbol * sexp) list)
    : (vname * lexp * ltype) list * elab_context =
  (* FIXME: Generalize when/where possible, so things like `map` can be
     defined without type annotations!  *)
  (* Preserve the new operators added to nctx.  *)
  let ectx = let (_,   a, b, c) = ectx in
             let (grm, _, _, _) = nctx in
             (grm, a, b, c) in
  let (declmap, nctx)
    = List.fold_right
                  (fun ((l, vname), pexp) (map, nctx) ->
                    let i = senv_lookup vname nctx in
                    assert (i < List.length defs);
                    match Myers.nth i (ectx_to_lctx nctx) with
                    | (v', ForwardRef, t)
                      -> let adjusted_t = push_susp t (S.shift (i + 1)) in
                        let e = check pexp adjusted_t nctx in
                        let (grm, ec, lc, sl) = nctx in
                        let d = (v', LetDef (i + 1, e), t) in
                        (IMap.add i ((l, Some vname), e, t) map,
                         (grm, ec, Myers.set_nth i d lc, sl))
                    | _ -> Log.internal_error "Defining same slot!")
                  defs (IMap.empty, nctx) in
  let decls = List.rev (List.map (fun (_, d) -> d) (IMap.bindings declmap)) in
  decls, ctx_define_rec ectx decls

and infer_and_generalize_type (ctx : elab_context) se name =
  let nctx = ectx_new_scope ctx in
  let t = infer_type se nctx name in
  (* We should not generalize over metavars which only occur on the rightmost
   * side of arrows in type annotations (aka declarations), since there's no
   * way for the callee to return something of the proper type if those
   * metavars don't also occur somewhere in the arguments.
   * E.g. for annotations like
   *
   *     x : ?;
   *     F : Type ? → Type ?;
   *
   * it makes no sense to generalize them to:
   *
   *     x : (ℓ : TypeLevel) ≡> (t : Type ℓ) ≡> t;
   *     F : (ℓ₁ : TypeLevel) ≡> (ℓ₂ : TypeLevel) ≡> Type ℓ₁ → Type ℓ₂;
   *
   * since there can't be corresponding definitions.  So replace the final
   * return type with some arbitrary closed constant before computing the
   * set of free metavars.
   * The same argument holds for other *positive* positions, e.g.
   *
   *     f : (? -> Int) -> Int;
   *
   * But we don't bother trying to catch all cases currently.
   *)
  let rec strip_rettype t =
    match lexp_lexp' t with
    | Arrow (ak, v, t1, l, t2)
     -> mkArrow (ak, v, t1, l, strip_rettype t2)
    | Sort _ | Metavar _ -> type0 (* Abritrary closed constant.  *)
    | _ -> t in
  let g = generalize nctx (strip_rettype t) in
  g (fun _ne name t l e
     -> mkArrow (Aerasable, name, t, l, e))
    t

and infer_and_generalize_def (ctx : elab_context) se =
  let nctx = ectx_new_scope ctx in
  let (e,t) = infer se nctx in
  let g = generalize nctx e in
  let e' = g (fun ne vname t l e
              -> mkLambda ((if ne then Aimplicit else Aerasable),
                          vname, t, e))
             e in
  let t' = g (fun ne name t l e
              -> mkArrow ((if ne then Aimplicit else Aerasable),
                         name, t, sexp_location se, e))
             t in
  (e', t')

and lexp_decls_1
      (sdecls : sexp list)                        (* What's already parsed *)
      (tokens : token list)                       (* Rest of input *)
      (ectx : elab_context)                       (* External ctx.  *)
      (nctx : elab_context)                       (* New context.  *)
      (pending_decls : location SMap.t)           (* Pending type decls. *)
      (pending_defs : (symbol * sexp) list)       (* Pending definitions. *)
    : (vname * lexp * ltype) list * sexp list * token list * elab_context =

  let rec lexp_decls_1 sdecls tokens nctx pending_decls pending_defs =
    let sdecl, sdecls, toks =
      match (sdecls, tokens) with
      | (s :: sdecls, _) -> Some s, sdecls, tokens
      | ([], []) -> None, [], []
      | ([], _) ->
         let (s, toks) = sexp_parse_all (ectx_get_grammar nctx)
                           tokens (Some ";") in
         Some s, [], toks in
    let recur prepend_sdecls nctx pending_decls pending_defs =
      lexp_decls_1 (List.append prepend_sdecls sdecls)
        toks nctx pending_decls pending_defs in
    match sdecl with
    | None -> (if not (SMap.is_empty pending_decls) then
                 let (s, loc) = SMap.choose pending_decls in
                 error ~loc ("Variable `" ^ s ^ "` declared but not defined!")
               else
                 assert (pending_defs == []));
              [], [], [],  nctx

    | Some (Symbol (_, ""))
      -> recur [] nctx pending_decls pending_defs

    | Some (Node (Symbol (_, ("_;_" (* | "_;" | ";_" *))), sdecls'))
      -> recur sdecls' nctx pending_decls pending_defs

    | Some (Node (Symbol (loc, "_:_"), args) as thesexp)
      (* FIXME: Move this to a "special form"!  *)
      -> (match args with
          | [Symbol (loc, vname); stp]
            -> let ltp = infer_and_generalize_type nctx stp (loc, Some vname) in
               if SMap.mem vname pending_decls then
                 (* Don't burp: take'em all and unify!  *)
                 let pt_idx = senv_lookup vname nctx in
                 (* Take the previous type annotation.  *)
                 let pt = match Myers.nth pt_idx (ectx_to_lctx nctx) with
                   | (_, ForwardRef, t) -> push_susp t (S.shift (pt_idx + 1))
                   | _ -> Log.internal_error "Var not found at its index!" in
                 (* Unify it with the new one.  *)
                 let _ = match Unif.unify ltp pt (ectx_to_lctx nctx) with
                   | (_::_)
                     -> lexp_error loc ltp
                          ("New type annotation `"
                           ^ lexp_string ltp ^ "` incompatible with previous `"
                           ^ lexp_string pt ^ "`")
                   | [] -> () in
                 recur [] nctx pending_decls pending_defs
               else if List.exists (fun ((_, vname'), _) -> vname = vname')
                         pending_defs then
                 (error ~loc ("Variable `" ^ vname ^ "` already defined!");
                  recur [] nctx pending_decls pending_defs)
               else recur [] (ectx_extend nctx (loc, Some vname) ForwardRef ltp)
                      (SMap.add vname loc pending_decls)
                      pending_defs
          | _ -> error ~loc ("Invalid type declaration syntax : `" ^
                               (sexp_string thesexp) ^ "`");
                 recur [] nctx pending_decls pending_defs)

    | Some (Node (Symbol (l, "_=_") as head, args) as thesexp)
      (* FIXME: Move this to a "special form"!  *)
      -> (match args with
          | [Symbol ((l, vname)); sexp]
               when SMap.is_empty pending_decls
            -> assert (pending_defs == []);
               (* Used to be true before we added define-operator.  *)
               (* assert (ectx == nctx); *)
               let (lexp, ltp) = infer_and_generalize_def nctx sexp in
               let var = (l, Some vname) in
               (* Lexp decls are always recursive, so we have to shift by 1 to
                * account for the extra var (ourselves).  *)
               [(var, mkSusp lexp (S.shift 1), ltp)], sdecls, toks,
               ctx_define nctx var lexp ltp

          | [Symbol (l, vname); sexp]
            -> if SMap.mem vname pending_decls then
                 let decl_loc = SMap.find vname pending_decls in
                 let v = ({file = l.file;
                           line = l.line;
                           column = l.column;
                           docstr = String.concat "\n" [decl_loc.docstr;
                                                        l.docstr]},
                          vname) in
                 let pending_decls = SMap.remove vname pending_decls in
                 let pending_defs = ((v, sexp) :: pending_defs) in
                 if SMap.is_empty pending_decls then
                   let nctx = ectx_new_scope nctx in
                   let decls, nctx = lexp_check_decls ectx nctx pending_defs in
                   decls, sdecls, toks, nctx
                 else
                   recur [] nctx pending_decls pending_defs

               else
                 (error ~loc:l ("`" ^ vname ^ "` defined but not declared!");
                  recur [] nctx pending_decls pending_defs)

          | [Node (Symbol s, args) as d; body]
            -> (* FIXME: Make it a macro (and don't hardcode `lambda_->_`)!  *)
             recur [Node (head,
                          [Symbol s;
                           Node (Symbol (sexp_location d, "lambda_->_"),
                                 [sexp_u_list args; body])])]
               nctx pending_decls pending_defs

          | _ -> error ~loc:l ("Invalid definition syntax : `" ^
                                 (sexp_string thesexp) ^ "`");
                 recur [] nctx pending_decls pending_defs)

    | Some (Node (Symbol (l, "define-operator"), args))
      (* FIXME: Move this to a "special form"!  *)
      -> recur [] (sdform_define_operator nctx l args None)
           pending_decls pending_defs

    | Some (Node (Symbol ((l, _) as v), sargs))
      -> (* expand macro and get the generated declarations *)
       let sdecl' = lexp_decls_macro v sargs nctx in
       recur [sdecl'] nctx pending_decls pending_defs

    | Some sexp
      -> error ~loc:(sexp_location sexp) "Invalid declaration syntax";
         recur [] nctx pending_decls pending_defs

  in (EV.set_getenv nctx;
      let res = lexp_decls_1 sdecls tokens nctx
                  pending_decls pending_defs in
        (Log.stop_on_error (); res))

and lexp_p_decls (sdecls : sexp list) (tokens : token list) (ctx : elab_context)
    : ((vname * lexp * ltype) list list * elab_context) =
  let rec impl sdecls tokens ctx =
    match (sdecls, tokens) with
    | ([], []) -> [], ectx_new_scope ctx
    | _ ->
       let decls, sdecls, tokens, nctx =
         lexp_decls_1 sdecls tokens ctx ctx SMap.empty [] in
       Log.stop_on_error ();
       let declss, nnctx = impl sdecls tokens nctx in
       decls :: declss, nnctx in
  impl sdecls tokens ctx

and lexp_parse_all (p: sexp list) (ctx: elab_context) : lexp list =
  let res = List.map (fun pe -> let e, _ = infer pe ctx in e) p in
    (Log.stop_on_error (); res)

and lexp_parse_sexp (ctx: elab_context) (e : sexp) : lexp =
  let e, _ = infer e ctx in (Log.stop_on_error (); e)

(* --------------------------------------------------------------------------
 *  Special forms implementation
 * -------------------------------------------------------------------------- *)

and sform_declexpr ctx loc sargs ot =
  match List.map (lexp_parse_sexp ctx) sargs with
  | [e] when is_var e
    -> (match DB.env_lookup_expr ctx ((loc, U.get_vname_name_option (get_var_vname (get_var e))), get_var_db_index (get_var e)) with
        | Some lxp -> (lxp, Lazy)
        | None -> error ~loc "no expr available";
                  sform_dummy_ret ctx loc)
  | _ -> error ~loc "declexpr expects one argument";
         sform_dummy_ret ctx loc


let sform_decltype ctx loc sargs ot =
  match List.map (lexp_parse_sexp ctx) sargs with
  | [e] when is_var e
    -> (DB.env_lookup_type ctx ((loc, U.get_vname_name_option (get_var_vname (get_var e))), get_var_db_index (get_var e)), Lazy)
  | _ -> error ~loc "decltype expects one argument";
         sform_dummy_ret ctx loc



let builtin_value_types : ltype option SMap.t ref = ref SMap.empty

let sform_built_in ctx loc sargs ot =
  match !parsing_internals, sargs with
  | true, [String (_, name)]
    -> (match ot with
       | Some ltp
         (* FIXME: This `L.clean` is basically the last remaining use of the
          * function.  It's not indispensible, tho it might still be useful for
          * performance of type-inference (at least until we have proper
          * memoization of push_susp and/or whnf).  *)
         -> let ltp' = L.clean (OL.lexp_close (ectx_to_lctx ctx) ltp) in
           let bi = mkBuiltin ((loc, name), ltp') in
           if not (SMap.mem name (!EV.builtin_functions)) then
             sexp_error loc ("Unknown built-in `" ^ name ^ "`");
           BI.add_builtin_cst name bi;
           (bi, Checked)
       | None -> error ~loc "Built-in's type not provided by context!";
                sform_dummy_ret ctx loc)

  | true, _ -> error ~loc "Wrong Usage of `Built-in`";
              sform_dummy_ret ctx loc

  | false, _ -> error ~loc "Use of `Built-in` in user code";
               sform_dummy_ret ctx loc

let sform_datacons ctx loc sargs ot =
  match sargs with
  | [t; Symbol ((sloc, cname) as sym)]
    -> let idt, _ = infer t ctx in
      (mkCons (idt, sym), Lazy)

  | [_;_] -> sexp_error loc "Second arg of ##constr should be a symbol";
            sform_dummy_ret ctx loc
  | _ -> sexp_error loc "##constr requires two arguments";
        sform_dummy_ret ctx loc

let elab_colon_to_ak k = match k with
  | "_:::_" -> Aerasable
  | "_::_" -> Aimplicit
  | _ -> Anormal

let elab_datacons_arg s = match s with
  | Node (Symbol (_, (("_:::_" | "_::_" | "_:_") as k)), [Symbol s; t])
    -> (elab_colon_to_ak k, elab_p_id s, t)
  | _ -> (Anormal, (sexp_location s, None), s)

let elab_typecons_arg arg : (arg_kind * vname * sexp option) =
  match arg with
  | Node (Symbol (_, (("_:::_" | "_::_" | "_:_") as k)), [Symbol (l,name); e])
    -> (elab_colon_to_ak k,
       (l, Some name), Some e)
  | Symbol (l, name) -> (Anormal, (l, Some name), None)
  | _ -> sexp_error ~print_action:(fun _ -> sexp_print arg; print_newline ())
           (sexp_location arg)
           "Unrecognized formal arg";
         (Anormal, (sexp_location arg, None), None)

let sform_typecons ctx loc sargs ot =
  match sargs with
  | [] -> sexp_error loc "No arg to ##typecons!"; (mkDummy_type ctx loc, Lazy)
  | formals :: constrs
    -> let (label, formals) = match formals with
        | Node (label, formals) -> (label, formals)
        | _ -> (formals, []) in
      let label = match label with
        | Symbol label -> label
        | _ -> let loc = sexp_location label in
              sexp_error loc "Unrecognized inductive type name";
              (loc, "<error>") in

      let rec parse_formals sformals rformals ctx = match sformals with
        | [] -> (List.rev rformals, ctx)
        | sformal :: sformals
          -> let (kind, var, opxp) = elab_typecons_arg sformal in
            let ltp = match opxp with
              | Some pxp -> let (l,_) = infer pxp ctx in l
              | None -> let (l,_) = var in
                       newMetatype (ectx_to_lctx ctx)
                                   (ectx_to_scope_level ctx) l in

            parse_formals sformals ((kind, var, ltp) :: rformals)
                          (ectx_extend ctx var Variable ltp) in

      let (formals, nctx) = parse_formals formals [] ctx in

      let ctors
        = List.fold_right
          (fun case pcases
           -> match case with
             (* read Constructor name + args => Type ((Symbol * args) list) *)
             | Node (Symbol s, cases)
               -> (s, List.map elab_datacons_arg cases)::pcases
             (* This is a constructor with no args *)
             | Symbol s -> (s, [])::pcases

             | _ -> sexp_error (sexp_location case)
                              "Unrecognized constructor declaration";
                   pcases)
          constrs [] in

      let map_ctor = lexp_parse_inductive ctors nctx in
      (mkInductive (loc, label, formals, map_ctor), Lazy)

let sform_hastype ctx loc sargs ot =
  match sargs with
  | [se; st] -> let lt = infer_type st ctx (loc, None) in
               let le = check se lt ctx in
               (le, Inferred lt)
  | _ -> sexp_error loc "##_:_ takes two arguments";
        sform_dummy_ret ctx loc

let sform_arrow kind ctx loc sargs ot =
  match sargs with
  | [st1; st2]
    -> let (v, st1) = match st1 with
        | Node (Symbol (_, "_:_"), [Symbol v; st1]) -> (elab_p_id v, st1)
        | _ -> ((sexp_location st1, None), st1) in
      let lt1 = infer_type st1 ctx v in
      let nctx = ectx_extend ctx v Variable lt1 in
      let lt2 = infer_type st2 nctx (sexp_location st2, None) in
      (mkArrow (kind, v, lt1, loc, lt2), Lazy)
  | _ -> sexp_error loc "##_->_ takes two arguments";
        sform_dummy_ret ctx loc

let sform_immediate ctx loc sargs ot =
  match sargs with
  | [(String _) as se]  -> mkImm (se), Inferred DB.type_string
  | [(Integer _) as se] -> mkImm (se), Inferred DB.type_int
  | [(Float _) as se]   -> mkImm (se), Inferred DB.type_float
  | [Block (sl, pts, el)]
    -> let grm = ectx_get_grammar ctx in
      let tokens = lex default_stt pts in
      let (se, _) = sexp_parse_all grm tokens None in
      elaborate ctx se ot
  | [se]
    -> (sexp_error loc ("Non-immediate passed to ##typer-immediate");
       sform_dummy_ret ctx loc)
  | _
    -> (sexp_error loc ("Too many args to ##typer-immediate");
       sform_dummy_ret ctx loc)


let sform_identifier ctx loc sargs ot =
  match sargs with
  | [Symbol (l,name)]
       when String.length name >= 1 && String.get name 0 == '#'
    -> if String.length name > 2 && String.get name 1 == '#' then
        let name = string_sub name 2 (String.length name) in
        try let (e,t) = SMap.find name (! BI.lmap) in
            (e, Inferred t)
        with Not_found
             -> sexp_error l ("Unknown builtin `" ^ name ^ "`");
               sform_dummy_ret ctx loc
      else (sexp_error l ("Invalid special identifier `" ^ name ^ "`");
            sform_dummy_ret ctx loc)

  | [Symbol (loc, name)]
       when String.length name >= 1 && String.get name 0 = '?'
    -> let name = if name = "?" then "" else
                   string_sub name 1 (String.length name) in
      (* Shift the var so it can't refer to the local vars.
       * This is used so that in cases like "lambda t (y : ?) ... "
       * type inference can guess ? without having to wonder whether it
       * can refer to `t` or not.  If the user wants ? to be able to refer
       * to `t`, then they should explicitly write (y : ? t).  *)
      let ctx_shift = ectx_local_scope_size ctx in
      let octx = Myers.nthcdr ctx_shift (ectx_to_lctx ctx) in
      let sl = ectx_to_scope_level ctx in
      let subst = S.shift ctx_shift in
      let (_, _, rmmap) = ectx_get_scope ctx in
      if not (name = "") && SMap.mem name (!rmmap) then
        let idx = SMap.find name (!rmmap) in
        match (metavar_lookup idx) with
        | MVar (sl',_,_)
          -> if sl = sl' then
               (mkMetavar (idx, subst, (loc, Some name)), Lazy)
             else
               (* FIXME: The variable is from another scope_level! It
                  means that `subst` is not the right substitution for
                  the metavar! *)
               fatal ~loc ("Bug in the elaboration of a metavar"
                           ^ " repeated at a different scope level!")
        | MVal _
          -> (* FIXME: We face the same problem as above, but here,
                the situation is worse, we don't even know the scope
                level! *)
           fatal ~loc "Bug in the elaboration of a repeated metavar!"
      else
        let t = match ot with
          | None -> newMetatype octx sl loc
          | Some t
            (* `t` is defined in ctx instead of octx. *)
            -> Inverse_subst.apply_inv_subst t subst in
        let mv = newMetavar octx sl (loc, Some name) t in
        (if not (name = "") then
           let idx =
           match lexp_lexp' mv with
             | Metavar (idx, _, _) -> idx
             | _ -> fatal ~loc "newMetavar returned a non-Metavar" in
           rmmap := SMap.add name idx (!rmmap));
        (mkSusp mv subst,
         match ot with Some _ -> Checked | None -> Lazy)

  (* Normal identifier.  *)
  | [Symbol id] -> elab_varref ctx id

  | [se]
    -> (sexp_error loc ("Non-symbol passed to ##typer-identifier");
       sform_dummy_ret ctx loc)

  | _
    -> (sexp_error loc ("Too many args to ##typer-identifier");
       sform_dummy_ret ctx loc)

let rec sform_lambda kind ctx loc sargs ot =
  match sargs with
  | [sarg; sbody]
    -> let (arg, ost1) = match sarg with
         | Node (Symbol (_, "_:_"), [Symbol arg; st]) -> (elab_p_id arg, Some st)
         | Symbol arg -> (elab_p_id arg, None)
         | _ -> sexp_error (sexp_location sarg)
                           "Unrecognized lambda argument";
                ((dummy_location, None), None) in

       let olt1 = match ost1 with
         | Some st -> Some (infer_type st ctx arg)
         | _ -> None in

       let mklam lt1 olt2 =
         let nctx = ectx_extend ctx arg Variable lt1 in
         let olt2 = match olt2 with
           | None -> None
           (* Apply a "dummy" substitution which replace #0 with #0
            * in order to account for the fact that `arg` and `v`
            * might not be the same name!  *)
           | Some lt2
             -> Some (srename arg lt2) in
         let (lbody, alt) = elaborate nctx sbody olt2 in
         (mkLambda (kind, arg, lt1, lbody),
          match alt with
          | Inferred lt2 -> Inferred (mkArrow (kind, arg, lt1, loc, lt2))
          | _ -> alt) in

       (match ot with
        | None -> mklam (match olt1 with
                        | Some lt1 -> lt1
                        | None -> newMetatype (ectx_to_lctx ctx)
                                             (ectx_to_scope_level ctx) loc)
                       None
        (* Read var type from the provided type *)
        | Some t
          -> let lp = OL.lexp_whnf t (ectx_to_lctx ctx) in
            match lexp_lexp' lp with
            | Arrow (ak2, _, lt1, _, lt2) when ak2 = kind
              -> (match olt1 with
                 | None -> ()
                 | Some lt1'
                   -> unify_or_error (ectx_to_lctx ctx) lt1'
                        ~lxp_name:"parameter" lt1 lt1');
                mklam lt1 (Some lt2)

            | Arrow (ak2, v, lt1, _, lt2) when kind = Anormal
              (* `t` is an implicit arrow and `kind` is Anormal,
               * so auto-add a corresponding Lambda wrapper!
               * FIXME: This should be moved to a macro.  *)
              -> (* FIXME: Here we end up adding a local variable `v` whose
                 * name is not lexically present, so there's a risk of
                 * name capture.  We should make those vars anonymous?  *)
                let nctx = ectx_extend ctx v Variable lt1 in
                (* FIXME: Don't go back to sform_lambda, but use an internal
                 * loop to avoid re-computing olt1 each time.  *)
                let (lam, alt) = sform_lambda kind nctx loc sargs (Some lt2) in
                (mkLambda (ak2, v, lt1, lam),
                 match alt with
                 | Inferred lt2' -> Inferred (mkArrow (ak2, v, lt1, loc, lt2'))
                 | _ -> alt)

            | lt
              -> let (lt1, lt2) = unify_with_arrow ctx loc lp kind arg olt1
                in mklam lt1 (Some lt2))

  | _ -> sexp_error loc ("##lambda_"^(match kind with Anormal -> "->"
                                                   | Aimplicit -> "=>"
                                                   | Aerasable -> "≡>")
                        ^"_ takes two arguments");
        sform_dummy_ret ctx loc

let rec sform_case ctx loc sargs ot = match sargs with
  | [Node (Symbol (_, "_|_"), se :: scases)]
    -> let parse_case branch = match branch with
        | Node (Symbol (_, "_=>_"), [pat; code])
          -> (pexp_p_pat pat, code)
        | _ -> let l = (sexp_location branch) in
              sexp_error l "Unrecognized simple case branch";
              (Ppatsym (l, None), Symbol (l, "?")) in
      let pcases = List.map parse_case scases in
      let t = match ot with
        | Some t -> t
        | None -> newMetatype (ectx_to_lctx ctx) (ectx_to_scope_level ctx) loc in
      let le = check_case t (loc, se, pcases) ctx in
      (le, match ot with Some _ -> Checked | None -> Inferred t)

  (* In case there are no branches, pretend there was a | anyway.  *)
  | [e] -> sform_case ctx loc [Node (Symbol (loc, "_|_"), sargs)] ot
  | _ -> sexp_error loc "Unrecognized case expression";
        sform_dummy_ret ctx loc

let sform_letin ctx loc sargs ot = match sargs with
  | [sdecls; sbody]
    -> let declss, nctx = lexp_p_decls [sdecls] [] ctx in
      (* FIXME: Use `elaborate`.  *)
      let bdy, ltp = infer sbody (ectx_new_scope nctx) in
      let s = List.fold_left (OL.lexp_defs_subst loc) S.identity declss in
      (lexp_let_decls declss bdy nctx,
       Inferred (mkSusp ltp s))
  | _ -> sexp_error loc "Unrecognized let_in_ expression";
        sform_dummy_ret ctx loc

let rec infer_level ctx se : lexp =
  match se with
  | Symbol (_, "z") -> mkSortLevel SLz
  | Symbol _ -> check se type_level ctx
  | Node (Symbol (_, "s"), [se])
    -> mkSortLevel (SLsucc (infer_level ctx se))
  | Node (Symbol (_, "_∪_"), [se1; se2])
    -> OL.mkSLlub (ectx_to_lctx ctx) (infer_level ctx se1) (infer_level ctx se2)
  | _ -> let l = (sexp_location se) in
        (sexp_error l ("Unrecognized TypeLevel: " ^ sexp_string se);
         newMetalevel (ectx_to_lctx ctx) (ectx_to_scope_level ctx) l)

(* Actually `Type_` could also be defined as a plain constant
 *     Lambda("l", TypeLevel, Sort (Stype (Var "l")))
 * But it would be less efficient (such a lambda can't be passed as argument
 * so it really can only be used applied to something, so it always generates
 * β-redexes.  Furthermore, I'm not sure if my PTS definition is correct to
 * allow such a lambda.  *)
let sform_type ctx loc sargs ot =
  match sargs with
  | [se] -> let l = infer_level ctx se in
           (mkSort (loc, Stype l),
            Inferred (mkSort (loc, Stype (mkSortLevel (mkSLsucc l)))))
  | _ -> (sexp_error loc "##Type_ expects one argument";
         sform_dummy_ret ctx loc)

let sform_debruijn ctx loc sargs ot =
  match sargs with
  | [Integer (l,i)]
    -> let i = Z.to_int i in if i < 0 || i > get_size ctx then
        (sexp_error l "##DeBruijn index out of bounds";
         sform_dummy_ret ctx loc)
      else
        let lxp = mkVar ((loc, None), i) in (lxp, Lazy)
  | _ -> (sexp_error loc "##DeBruijn expects one integer argument";
         sform_dummy_ret ctx loc)

(*  Only print var info *)
let lexp_print_var_info ctx =
    let ((m, _), env, _) = ctx in
    let n = Myers.length env in

    for i = 0 to n - 1 do (
        let (_, (_, name), exp, tp) = Myers.nth i env in
        print_int i; print_string " ";
        print_string name; (*   name must match *)
        print_string " = ";
         (match exp with
             | None -> print_string "<var>"
             | Some exp -> lexp_print exp);
        print_string ": ";
        lexp_print tp;
        print_string "\n")
    done

let in_pervasive = ref true

(*  arguments :
      elab_context from where load is called,
      loc is location of load call,
      sargs should be an array of one file name,
      ot is the expected type of output tuple.
*)
let sform_load usr_elctx loc sargs ot =

  let read_file file_name elctx =
    let pres =
      try prelex_file file_name with
      | Sys_error _
        -> error ~loc
             ("Could not load `" ^ file_name ^ "`: file not found."); [] in
    let sxps = lex default_stt pres in
    let _, elctx = lexp_p_decls [] sxps elctx
    in elctx in

  (* read file as elab_context *)
  let ld_elctx = match sargs with
    | [String (_,file_name)] -> if !in_pervasive then
        read_file file_name usr_elctx
      else
        read_file file_name !sform_default_ectx
    | _ -> (error ~loc "argument to load should be one file name (String)";
            !sform_default_ectx) in

  (* get lexp_context *)
  let usr_lctx  = ectx_to_lctx usr_elctx in
  let ld_lctx   = ectx_to_lctx ld_elctx in
  let dflt_lctx = ectx_to_lctx !sform_default_ectx in

  (* length of some lexp_context *)
  let usr_len   = M.length usr_lctx in
  let dflt_len  = M.length dflt_lctx in

  (* create a tuple from context and shift it to user context         *
   *  also check if we are in pervasive in which case                 *
   *  we want to load in the current context rather than the default  *)
  let tuple = if !in_pervasive then
      OL.ctx2tup usr_lctx ld_lctx
    else
      OL.ctx2tup dflt_lctx ld_lctx in

  let tuple' = if !in_pervasive then
      tuple
    else
      (Lexp.mkSusp tuple (S.shift (usr_len - dflt_len))) in

  (tuple',Lazy)

(* Register special forms.  *)
let register_special_forms () =
  List.iter add_special_form
            [
              ("Built-in",      sform_built_in);
              ("DeBruijn",      sform_debruijn);
              ("typer-identifier", sform_identifier);
              ("typer-immediate", sform_immediate);
              ("datacons",      sform_datacons);
              ("typecons",      sform_typecons);
              ("_:_",           sform_hastype);
              ("lambda_->_",    sform_lambda Anormal);
              ("lambda_=>_",    sform_lambda Aimplicit);
              ("lambda_≡>_",   sform_lambda Aerasable);
              ("_->_",          sform_arrow Anormal);
              ("_=>_",          sform_arrow Aimplicit);
              ("_≡>_",         sform_arrow Aerasable);
              ("case_",         sform_case);
              ("let_in_",       sform_letin);
              ("Type_",         sform_type);
              ("load",         sform_load);
              (* FIXME: We should add here `let_in_`, `case_`, etc...  *)
              (* FIXME: These should be functions!  *)
              ("decltype",      sform_decltype);
              ("declexpr",      sform_declexpr);
            ]

(*      Default context with builtin types
 * --------------------------------------------------------- *)

let dynamic_bind r v body =
  let old = !r in
  try r := v;
      let res = body () in
      r := old;
      res
  with e -> r := old; raise e

(* Make lxp context with built-in types *)
let default_ectx
  = try let _ = register_special_forms () in

    (* Read BTL files *)
    let read_file file_name elctx =
      let pres = prelex_file file_name in
      let sxps = lex default_stt pres in
      let _, lctx = lexp_p_decls [] sxps elctx
      in lctx in

    (* Register predef *)
    let register_predefs elctx =
      try List.iter (fun name ->
            let idx = senv_lookup name elctx in
            let v = mkVar ((dloc, Some name), idx) in
              BI.set_predef name v) BI.predef_names;
      with Senv_Lookup_Fail _ ->
        warning "Predef not found"; in

    (* Empty context *)
    let lctx = empty_elab_context in
    let lctx = SMap.fold (fun key (e, t) ctx
                          -> if String.get key 0 = '-' then ctx
                            else ctx_define ctx (dloc, Some key) e t)
                         (!BI.lmap) lctx in

    Heap.register_builtins ();
    (* read base file *)
    let lctx = dynamic_bind parsing_internals true
                            (fun ()
                             -> read_file (btl_folder ^ "/builtins.typer")
                                          lctx) in
    let _ = register_predefs lctx in

    (* Does not work, not sure why
    let files = ["list.typer"; "quote.typer"; "type.typer"] in
    let lctx = List.fold_left (fun lctx file_name ->
      read_file (btl_folder ^ "/" ^ file_name) lctx) lctx files in *)


    builtin_size := get_size lctx;
    let ectx = dynamic_bind in_pervasive true
                 (fun () -> read_file (btl_folder ^ "/pervasive.typer") lctx) in
    let _ = sform_default_ectx := ectx in
    ectx
  with e ->
    error "Compilation stopped in default context";
    Log.print_and_clear_log ();
    raise e

let default_rctx = EV.from_ectx default_ectx

(*      String Parsing
 * --------------------------------------------------------- *)

let lexp_expr_str str ctx =
  let tenv = default_stt in
  let grm = ectx_get_grammar ctx in
  let limit = Some ";" in
  let pxps = sexp_parse_str str tenv grm limit in
  let lexps = lexp_parse_all pxps ctx in
  List.iter (fun lxp -> ignore (OL.check (ectx_to_lctx ctx) lxp))
    lexps;
  lexps

let lexp_decl_str str ctx =
  let tenv = default_stt in
  let tokens = lex_str str tenv in
  lexp_p_decls [] tokens ctx


(*  Eval String
 * --------------------------------------------------------- *)
(* Because we cant include Elab in eval.ml *)

let eval_expr_str str lctx rctx =
  let lxps = lexp_expr_str str lctx in
  let elxps = List.map OL.erase_type lxps in
  EV.eval_all elxps rctx false

let eval_decl_str str lctx rctx =
  let lxps, lctx = lexp_decl_str str lctx in
  let elxps = List.map OL.clean_decls lxps in
  (EV.eval_decls_toplevel elxps rctx), lctx
