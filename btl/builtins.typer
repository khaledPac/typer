%%% builtins.typer --- Initialize the builtin functions

%%    Copyright (C) 2011-2020  Free Software Foundation, Inc.
%%
%% Author: Pierre Delaunay <pierre.delaunay@hec.ca>
%% Keywords: languages, lisp, dependent types.
%%
%% This file is part of Typer.
%%
%% Typer is free software; you can redistribute it and/or modify it under the
%% terms of the GNU General Public License as published by the Free Software
%% Foundation, either version 3 of the License, or (at your option) any
%% later version.
%%
%% Typer is distributed in the hope that it will be useful, but WITHOUT ANY
%% WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
%% FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
%% more details.
%%
%% You should have received a copy of the GNU General Public License along
%% with this program.  If not, see <http://www.gnu.org/licenses/>.

%%% Commentary:

%% Define builtin types and functions.
%% This file MUST be correctly typed, in the sense that many of the types in
%% here are "axioms" that the type checker must accept without being
%% able to verify them.

%%% Code:

%%%% Base Types used in builtin functions

%% Box : Type_ ?ℓ₁ -> Type_ ?ℓ₂;
%% Box = lambda (ℓ₃ : TypeLevel) ≡> typecons (Box (t : Type_ ℓ₃)) (box t);

%% TypeLevel_succ = Built-in "TypeLevel.succ" : TypeLevel -> TypeLevel;
%% TypeLevel_⊔ = Built-in "TypeLevel.⊔" : TypeLevel -> TypeLevel -> TypeLevel;

%% The trivial type which carries no information.
Unit    = typecons Unit unit;
unit    = datacons Unit unit;
() = unit;

%% The empty type, with no constructors: nothing can have this type.
Void = typecons Void;

%% Eq : (l : TypeLevel) ≡> (t : Type_ l) ≡> t -> t -> Type_ l
%% Eq' : (l : TypeLevel) ≡> Type_ l -> Type_ l -> Type_ l
Eq_refl : ((x : ?t) ≡> Eq x x);
Eq_refl = ##Eq\.refl;

Eq_cast : (x : ?) ≡> (y : ?)
          ≡> (p : Eq x y)
          ≡> (f : ? -> ?)
          ≡> f x -> f y;
%% FIXME: I'd like to just say:
%%     Eq_cast : Eq ?x ?y ≡> ?f ?x -> ?f ?y;
Eq_cast = Built-in "Eq.cast";

%% Commutativity of equality!
%% FIXME: I'd like to just say:
%%     Eq_comm : Eq ?x ?y -> Eq ?y ?x`;
Eq_comm : (x : ?t) ≡> (y : ?t) ≡> Eq x y -> Eq y x;
Eq_comm p = Eq_cast (f := lambda xy -> Eq xy x)
                    %% FIXME: The code is incorrectly accepted even
                    %% without this `(p := p)` because we just get a
                    %% metavar which remains uninstantiated and then
                    %% the definition gets ignored in the runtime
                    %% environment (see Eval.from_lctx).
                    (p := p)
                    Eq_refl;

%% General recursion!!
%% Whether this breaks consistency or not is a good question.
%% The basic idea is the following:
%%
%%   The `witness` argument presumably makes sure that `Y f` can only
%%   create new recursive values for types which were already inhabited.
%%   So there's no `f` such that `Y f : False`.
%%
%% But this is not sufficient, because you could use `Y` to create
%% new recursive *types* which then let you construct new arbitrary
%% recursive values of previously non-existent types.
%% E.g. you could create `Y <something> = ((... → t) -> t) -> t`
%% and then give the term `λx. x x` inhabiting that type, and from that
%% get a proof of False.
%%
%% So we have a secondary restriction: This `Y` is a builtin primitive/axiom
%% with no reduction rule, so that `Y <something>` is never convertible
%% to something like `((... → t) -> t) -> t`
%%
%% Of course, we do have a "natural" evaluation rule for it, so after type
%% checking we can run our recursive functions just fine, but those
%% recursive functions won't be unfolded during type checking.
%%
%% FIXME: Really, this Y should be used internally/transparently for any
%% function which has not been termination-checked successfully (or at all).
Y : (witness : ?a -> ?b) ≡> ((?a -> ?b) -> (?a -> ?b)) -> (?a -> ?b);
Y = Built-in "Y";

Bool = typecons (Bool) (true) (false);
true = datacons Bool true;
false = datacons Bool false;

%% Basic operators
Int_+ = Built-in "Int.+" : Int -> Int -> Int;
Int_- = Built-in "Int.-" : Int -> Int -> Int;
Int_* = Built-in "Int.*" : Int -> Int -> Int;
Int_/ = Built-in "Int./" : Int -> Int -> Int;

_+_ = Int_+;
_-_ = Int_-;
_*_ = Int_*;
_/_ = Int_/;

%% modulo
Int_mod = Built-in "Int.mod" : Int -> Int -> Int;

%% Operators on bits
Int_and = Built-in "Int.and" : Int -> Int -> Int;
Int_or = Built-in "Int.or" : Int -> Int -> Int;
Int_xor = Built-in "Int.xor" : Int -> Int -> Int;
Int_lsl = Built-in "Int.lsl" : Int -> Int -> Int;
Int_lsr = Built-in "Int.lsr" : Int -> Int -> Int;
Int_asr = Built-in "Int.asr" : Int -> Int -> Int;

Int_<  = Built-in "Int.<"  : Int -> Int -> Bool;
Int_>  = Built-in "Int.>"  : Int -> Int -> Bool;
Int_eq = Built-in "Int.="  : Int -> Int -> Bool;
Int_<= = Built-in "Int.<=" : Int -> Int -> Bool;
Int_>= = Built-in "Int.>=" : Int -> Int -> Bool;

%% bitwise negation
Int_not = Built-in "Int.not" : Int -> Int;

%% `Int` and `Integer` are conceptually isomorphic in that they both
%% represent unbounded integers, but in reality, both are bounded.
%% `Int` is implemented with `int` type in ocaml (31 or 63 bits), and
%% `Integer` is implemented with big integers (ultimately limited by
%% available memory). Both cause runtime errors at their limits, so no
%% overflows are allowed.  Here are the functions that witness the
%% isomorphism:
Int->Integer = Built-in "Int->Integer" : Int -> Integer;
Integer->Int = Built-in "Integer->Int" : Integer -> Int;

Int->String = Built-in "Int->String" : Int -> String;

Integer_+ = Built-in "Integer.+" : Integer -> Integer -> Integer;
Integer_- = Built-in "Integer.-" : Integer -> Integer -> Integer;
Integer_* = Built-in "Integer.*" : Integer -> Integer -> Integer;
Integer_/ = Built-in "Integer./" : Integer -> Integer -> Integer;

Integer_< =  Built-in "Integer.<"  : Integer -> Integer -> Bool;
Integer_> =  Built-in "Integer.>"  : Integer -> Integer -> Bool;
Integer_eq = Built-in "Integer.="  : Integer -> Integer -> Bool;
Integer_<= = Built-in "Integer.<=" : Integer -> Integer -> Bool;
Integer_>= = Built-in "Integer.>=" : Integer -> Integer -> Bool;

Integer->String = Built-in "Integer->String" : Integer -> String;

Float_+ = Built-in "Float.+" : Float -> Float -> Float;
Float_- = Built-in "Float.-" : Float -> Float -> Float;
Float_* = Built-in "Float.*" : Float -> Float -> Float;
Float_/ = Built-in "Float./" : Float -> Float -> Float;
Float->String = Built-in "Float->String" : Float -> String;

Float_< =  Built-in "Float.<"  : Float -> Float -> Bool;
Float_> =  Built-in "Float.>"  : Float -> Float -> Bool;
Float_eq = Built-in "Float.="  : Float -> Float -> Bool;
Float_<= = Built-in "Float.<=" : Float -> Float -> Bool;
Float_>= = Built-in "Float.>=" : Float -> Float -> Bool;

Float_trunc = Built-in "Float.trunc" : Float -> Float;

String_eq = Built-in "String.=" : String -> String -> Bool;

String_concat  = Built-in "String.concat" : String -> String -> String;
String_sub = Built-in "String.sub" : String -> Int -> Int -> String;

Sexp_eq   = Built-in   "Sexp.=" : Sexp -> Sexp -> Bool;

%%
%% Takes an Sexp
%% Returns the same Sexp in the IO monad
%%   but also print the Sexp to standard output
%%
Sexp_debug_print = Built-in "Sexp.debug_print" : Sexp -> IO Sexp;

%% -----------------------------------------------------
%%       List
%% -----------------------------------------------------

%% FIXME: "List : ?" should be sufficient but triggers
List : Type_ ?ℓ -> Type_ ?ℓ;
%% List a = typecons List (nil) (cons a (List a));
%% nil = lambda a ≡> datacons (List a) nil;
%% cons = lambda a ≡> datacons (List a) cons;
List = typecons (List (ℓ ::: TypeLevel) (a : Type_ ℓ)) (nil) (cons a (List a));
nil = datacons List nil;
cons = datacons List cons;

%%%% Macro-related definitions

Sexp_block   = Built-in "Sexp.block"   : String -> Sexp;
Sexp_symbol  = Built-in "Sexp.symbol"  : String    -> Sexp;
Sexp_string  = Built-in "Sexp.string"  : String    -> Sexp;
Sexp_node    = Built-in "Sexp.node"    : Sexp -> List Sexp -> Sexp;
Sexp_integer = Built-in "Sexp.integer" : Integer   -> Sexp;
Sexp_float   = Built-in "Sexp.float"   : Float     -> Sexp;

Macro = typecons (Macro)
                 (macro (List Sexp -> IO Sexp));
macro = datacons Macro macro;

Macro_expand : Macro -> List Sexp -> IO Sexp;
Macro_expand = lambda m -> lambda args -> case m
  | macro f => f args;

Sexp_dispatch : Sexp
                -> (node   : Sexp -> List Sexp -> ?a)
                -> (symbol : String -> ?a)
                -> (string : String -> ?a)
                -> (int    : Integer -> ?a)
                -> (float  : Float -> ?a)
                -> (block  : Sexp -> ?a)
                -> ?a ;
Sexp_dispatch = Built-in "Sexp.dispatch";

%%
%% Parse a block using the grammar in the passed context
%%
Reader_parse  = Built-in "Reader.parse"  : Elab_Context -> Sexp -> List Sexp;

%%%% Array (without IO, but they could be added easily)

%%
%% Takes an index, a new value and an array
%% Returns a copy of the array with the element
%%   at the specified index set to the new value
%%
%% Array_set is in O(N) where N is the length
%%
Array_set : Int -> ?a -> Array ?a -> Array ?a;
Array_set = Built-in "Array.set";

%%
%% Takes an element and an array
%% Returns a copy of the array with the new element at the end
%%
%% Array_append is in O(N) where N is the length
%%
Array_append : ?a -> Array ?a -> Array ?a;
Array_append = Built-in "Array.append";

%%
%% Takes an Int (n) and a value
%% Returns an array containing n times the value
%%
Array_create : Int -> ?a -> Array ?a;
Array_create = Built-in "Array.create";

%%
%% Takes an array
%% Returns the number of element in the array
%%
Array_length : Array ?a -> Int;
Array_length = Built-in "Array.length";

%%
%% Takes a default value, an index and an array
%% Returns the value in the array at the specified index or
%%   the default value if the index is out of bounds
%%
Array_get : ?a -> Int -> Array ?a -> ?a;
Array_get = Built-in "Array.get";

%%
%% It returns an empty array
%%
%% let Typer deduce the value of (a : Type)
%%
Array_empty : Unit -> Array ?a;
Array_empty = Built-in "Array.empty";

%%%% Monads

%% Builtin bind
IO_bind : IO ?a -> (?a -> IO ?b) -> IO ?b;
IO_bind = Built-in "IO.bind";

IO_return : ?a -> IO ?a;
IO_return = Built-in "IO.return";

%% `runIO` should have type IO Unit -> b -> b
%% or IO a -> b -> b, which means "run the IO, throw away the result,
%% and then return the second argument unchanged".  The "dummy" b argument
%% is actually crucial to make sure the result of runIO is used, otherwise
%% the whole call would look like a dead function call and could be
%% optimized away!
IO_run : IO Unit -> ?a -> ?a;
IO_run = Built-in "IO.run";

%% File monad

%% Define operations on file handle.
File_open   = Built-in "File.open"   : String -> String -> IO FileHandle;
File_stdout = Built-in "File.stdout" : Unit -> FileHandle;
File_write  = Built-in "File.write"  : FileHandle -> String -> IO Unit;
File_read   = Built-in "File.read"   : FileHandle -> Int -> IO String;

Sys_cpu_time = Built-in "Sys.cpu_time" : Unit -> IO Float;
Sys_exit     = Built-in "Sys.exit" : Int -> IO Unit;

%%
%% Ref (modifiable value)
%%
%% Conceptualy:
%%     Ref : Type -> Type;
%%     Ref = typecons (Ref (a : Type)) (Ref a);
%%

%%
%% Takes a value
%% Returns a value modifiable in the IO monad
%%   and which already contain the specified value
%%
Ref_make : ?a -> IO (Ref ?a);
Ref_make = Built-in "Ref.make";

%%
%% Takes a Ref
%% Returns in the IO monad the value in the Ref
%%
Ref_read : Ref ?a -> IO ?a;
Ref_read = Built-in "Ref.read";

%%
%% Takes a value and a Ref
%% Returns the an empty command
%%   and set the Ref to contain specified value
%%
Ref_write : ?a -> Ref ?a -> IO Unit;
Ref_write = Built-in "Ref.write";

%%
%% gensym for macro
%%
%% Generate pseudo-unique symbol
%%
%% At least they cannot be obtained outside macro.
%%   In macro you should NOT use symbol of the form " %gensym% ...".
%%   Assume the three dots to be anything.
%%
gensym = Built-in "gensym" : Unit -> IO Sexp;

%%%% Function on Elab_Context

%%
%% Get the current context of elaboration
%%
Elab_getenv = Built-in "Elab.getenv" : Unit -> IO Elab_Context;

%%
%% Check if a symbol is defined in a particular context
%%
Elab_isbound = Built-in "Elab.isbound" : String -> Elab_Context -> Bool;

%%
%% Check if a symbol is a constructor in a particular context
%%
Elab_isconstructor = Built-in "Elab.isconstructor"
  : String -> Elab_Context -> Bool;

%%
%% Check if the n'th field of a constructor is erasable
%% If the constructor isn't defined it will always return false
%%
Elab_is-nth-erasable = Built-in "Elab.is-nth-erasable" : String -> Int -> Elab_Context -> Bool;

%%
%% Check if a field of a constructor is erasable
%% If the constructor or the field aren't defined it will always return false
%%
Elab_is-arg-erasable = Built-in "Elab.is-arg-erasable" : String -> String -> Elab_Context -> Bool;

%%
%% Get n'th field of a constructor
%% It return "_" in case the field isn't defined
%% see pervasive.typer for a more convenient function
%%
Elab_nth-arg' = Built-in "Elab.nth-arg" : String -> Int -> Elab_Context -> String;

%%
%% Get the position of a field in a constructor
%% It return -1 in case the field isn't defined
%% see pervasive.typer for a more convenient function
%%
Elab_arg-pos' = Built-in "Elab.arg-pos" : String -> String -> Elab_Context -> Int;

%%
%% Get the docstring associated with a symbol
%%
Elab_debug-doc = Built-in "Elab.debug-doc" : String -> Elab_Context -> String;

%%%% Unit test helper IO

%%
%% Print message and/or fail (terminate)
%% And location is printed before the error
%%

%%
%% Voluntarily fail
%%   use case: next unit tests are not worth testing
%%
%% Takes a "section" and a "message" as argument
%%
Test_fatal   = Built-in "Test.fatal"   : String -> String -> IO Unit;

%%
%% Throw a warning, very similar to `Test_info`
%%   but it's possible to implement a parameter for user who want it to sometimes be fatal
%%
%% Takes a "section" and a "message" as argument
%%
Test_warning = Built-in "Test.warning" : String -> String -> IO Unit;

%%
%% Just print a message with location of the call
%%
%% Takes a "section" and a "message" as argument
%%
Test_info    = Built-in "Test.info"    : String -> String -> IO Unit;

%%
%% Get a string representing location of call ("file:line:column")
%%
Test_location = Built-in "Test.location" : Unit -> String;

%%
%% Do some test which print a message: "[  OK]" or "[FAIL]"
%%   followed by a message passed as argument
%%

%%
%% Takes a message and a boolean which should be true
%% Returns true if the boolean was true
%%
Test_true  = Built-in "Test.true"  : String -> Bool -> IO Bool;

%%
%% Takes a message and a boolean which should be false
%% Returns true if the boolean was false
%%
Test_false = Built-in "Test.false" : String -> Bool -> IO Bool;

%%
%% Takes a message and two arbitrary value of the same type
%% Returns true when the two value are equal
%%
Test_eq : String -> ?a -> ?a -> IO Bool;
Test_eq = Built-in "Test.eq";

%%
%% Takes a message and two arbitrary value of the same type
%% Returns true when the two value are not equal
%%
Test_neq : String -> ?a -> ?a -> IO Bool;
Test_neq = Built-in "Test.neq";

%%
%% Given a string, return an opaque DataconsLabel that identifies a data
%% constructor.
%%
datacons-label<-string : String -> ##DataconsLabel;
datacons-label<-string = Built-in "datacons-label<-string";

%%% builtins.typer ends here.
