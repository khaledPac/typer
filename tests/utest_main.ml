(*
 *      Typer Compiler
 *
 * ---------------------------------------------------------------------------
 *
 *      Copyright (C) 2011-2020  Free Software Foundation, Inc.
 *
 *   Author: Pierre Delaunay <pierre.delaunay@hec.ca>
 *   Keywords: languages, lisp, dependent types.
 *
 *   This file is part of Typer.
 *
 *   Typer is free software; you can redistribute it and/or modify it under the
 *   terms of the GNU General Public License as published by the Free Software
 *   Foundation, either version 3 of the License, or (at your option) any
 *   later version.
 *
 *   Typer is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 *   more details.
 *
 *   You should have received a copy of the GNU General Public License along
 *   with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ---------------------------------------------------------------------------
 *
 *      Description:
 *          Basic utest program run all tests
 *
 * --------------------------------------------------------------------------- *)

(** Search *_test.byte executable and run them.
    Usage:
        ./utest_main tests_folder root_folder *)

open Fmt

let global_verbose_lvl = ref 5
let global_sample_dir = ref "."
let global_tests_dir = ref (Filename.concat "_build" "tests")
let global_fsection = ref ""
let global_ftitle = ref ""
let global_filter = ref false

let arg_defs =
  [
    ("--verbose",
     Arg.Set_int global_verbose_lvl, " Set verbose level");
    ("--samples",
     Arg.Set_string global_sample_dir, " Set sample directory");
    ("--tests",
     Arg.Set_string global_tests_dir, " Set tests directory");
    (* Allow users to select which test to run *)
    ("--fsection",
     Arg.String (fun g -> global_fsection := String.uppercase_ascii g;
                          global_filter := true), " Set test filter");
    ("--ftitle",
     Arg.String (fun g -> global_ftitle := String.uppercase_ascii g;
                          global_filter := true), " Set test filter");
  ]


let verbose n = (n <= (!global_verbose_lvl))

let parse_args () = Arg.parse arg_defs (fun s -> ()) ""

let ut_string vb msg = if verbose vb then print_string msg else ()

let cut_byte str = String.sub str 0 ((String.length str) - 10)
let cut_native str = String.sub str 0 ((String.length str) - 12)

let cut_name str =
  if Filename.check_suffix str "_test.byte"
  then cut_byte str
  else cut_native str

let print_file_name i n name pass =
  let line_size = 80 - (String.length (cut_name name)) - 16 in
  let name = cut_name name in

  (if pass then print_string green else print_string red);
  print_string "    (";
  ralign_print_int i 2; print_string "/";
  ralign_print_int n 2; print_string ") ";
  print_string name;
  print_string (make_line '.' line_size);
  if pass then print_string "..OK\n" else print_string "FAIL\n";
  print_string reset

let must_run str =
  not !global_filter
  || String.uppercase_ascii (cut_name str) = !global_fsection

let main () =
  parse_args ();

  print_string  "\n";
  calign_print_string " Running Unit Test " 80;
  print_string  "\n\n";

  (*print_string ("[       ] Test folder: " ^ folder ^ "\n"); *)

  (* get tests files *)
  let folder = !global_tests_dir in
  let root_folder = !global_sample_dir in

  let files =
    try Sys.readdir folder
    with
    | e
      -> (print_string ("The folder: " ^ !global_tests_dir
                        ^ " does not exist.\n"
                        ^ "Have you tried \"./utests --tests= ./tests\" ?\n");
          raise e)
  in

  let check name =
    Filename.check_suffix name "_test.byte"
    || Filename.check_suffix name "_test.native"
  in

  (* select files that are executable tests *)
  let files = Array.fold_left
                (fun acc elem -> if check elem then elem::acc else acc)
                [] files in

  let files_n = List.length files in

  (if files_n = 0 then
     print_string "No tests were found. Did you compiled them? \n");

  let exit_code = ref 0 in
  let failed_test = ref 0 in
  let tests_n = ref 0 in
  let test_args =
    ["--samples"; root_folder;
     "--verbose"; string_of_int !global_verbose_lvl]
    @ (if not (!global_ftitle = "") then
         ["--ftitle"; !global_ftitle] else []) in

  let run_file test_path =
    flush stdout;

    tests_n := !tests_n + 1;
    let command_parts = Filename.concat folder test_path :: test_args in
    let command_parts =
      if Sys.os_type = "Win32"
      then
        (* I wish Filename.quote could be used on Windows.

           It would be appropriate if Ocaml spawned the process through the C
           API, but Sys.command passes does it through the shell, which is
           cmd.exe and has its own quoting rules. Ocaml has
           Filename.quote_command, which does the right thing on every platform,
           but it is only available since Ocaml 4.10. Until then, let's hope the
           command does not need to be escaped. *)
        command_parts
      else List.map Filename.quote command_parts
    in

    print_endline (String.concat " " command_parts);
    exit_code := Sys.command (String.concat " " command_parts);

    (if !exit_code != 0
     then
       ((if verbose 1 then print_file_name !tests_n files_n test_path false);
        failed_test := !failed_test + 1)
     else
       (if verbose 1 then print_file_name !tests_n files_n test_path true);
    );

    (if verbose 2 then print_newline ());
  in

  List.iter run_file (List.filter must_run files);

  print_string   "\n\n";
  print_string   "    Test Ran    : "; print_int !tests_n;
  print_string "\n    Test Failed : "; print_int !failed_test;
  print_string "\n    Test Passed : "; print_int (!tests_n - !failed_test);
  print_endline "\n"

let _ = main ();
